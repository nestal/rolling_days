#pragma once

#include <string>

class Network
{
private:
	Network();
	~Network();
public:
	static auto& instance()
	{
		static const Network inst;
		return inst;
	}

	[[nodiscard]] std::string hostname() const;
};


