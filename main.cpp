#include <iostream>
#include <filesystem>
#include <iomanip>
#include <sstream>
#include <optional>
#include <map>
#include <set>

#include "date/date.h"
#include "Network.h"

namespace fs = std::filesystem;

std::optional<std::chrono::system_clock::time_point> parse_time(const std::string& prefix, std::string name)
{
	if (name.substr(0, prefix.size()) == prefix)
		name.erase(0, prefix.size());

	std::istringstream iss{name};
	struct tm tm{};
	tm.tm_isdst = -1;
	iss >> std::get_time(&tm, "%H%M%S-%d%m%Y");

	if (!iss)
		return std::nullopt;

	return std::chrono::system_clock::from_time_t(std::mktime(&tm));
}

struct YearMonthWeekIndex
{
public:
	YearMonthWeekIndex(date::year y, date::month m, unsigned week) :
		m_year{y}, m_month{m}, m_week{week}
	{
	}

	[[nodiscard]] auto year() const {return m_year;}
	[[nodiscard]] auto month() const {return m_month;}
	[[nodiscard]] auto week() const {return m_week;}

private:
	date::year	m_year;
	date::month	m_month;
	unsigned	m_week;
};

bool operator<(const YearMonthWeekIndex& y1, const YearMonthWeekIndex& y2)
{
	return y1.year() < y2.year() ? true
		: (y1.year() > y2.year() ? false
		: (y1.month() < y2.month() ? true
		: (y1.month() > y2.month() ? false
		: y1.week() < y2.week())));
}

std::ostream& operator<<(std::ostream& os, const YearMonthWeekIndex& ymwi)
{
	return os << ymwi.year() << '-' << ymwi.month() << "-week#" << ymwi.week();
}

class Entry
{
public:
	explicit Entry(fs::path path, std::chrono::system_clock::time_point file_time) :
		m_path{std::move(path)},
		m_file_time{file_time}
	{
	}

	[[nodiscard]] static std::optional<Entry> from_path(const std::string& prefix, const fs::path& path)
	{
		if (auto ftime = parse_time(prefix, path.filename().string()); ftime)
			return Entry{path, *ftime};
		else
			return std::nullopt;
	}

	[[nodiscard]] auto& path() const {return m_path;}
	[[nodiscard]] auto& file_time() const {return m_file_time;}

	template <typename Date>
	[[nodiscard]] Date get_date() const
	{
		date::year_month_day ymd{date::floor<date::days>(m_file_time)};
		if constexpr (std::is_same_v<Date, date::year_month>)
			return date::year_month{ymd.year(), ymd.month()};
		else if constexpr (std::is_same_v<Date, date::year>)
			return date::year{ymd.year()};
		else if constexpr (std::is_same_v<Date, YearMonthWeekIndex>)
		{
			date::year_month_weekday ymw{date::floor<date::days>(m_file_time)};
			return YearMonthWeekIndex{ymw.year(), ymw.month(), ymw.index()};
		}
		else
			return ymd;
	}

private:
	fs::path	m_path;
	std::chrono::system_clock::time_point m_file_time;
};


template <typename Bucket>
bool put_in_bucket(const Entry& entry, Bucket& bucket)
{
	auto key = entry.get_date<typename Bucket::key_type>();

	auto [it, inserted] = bucket.try_emplace(key, entry);
	if (!inserted)
	{
		if (it->second.file_time() < entry.file_time())
		{
			it->second = entry;
			inserted = true;
		}
	}
	return inserted;
}

int main(int argc, char  **argv)
{
	// put the files we want to keep in buckets
	std::map<date::year_month_day, Entry> day_bucket;
	std::map<YearMonthWeekIndex, Entry> week_bucket;
	std::map<date::year_month, Entry> month_bucket;
	std::map<date::year, Entry> year_bucket;

	// files that we don't need
	std::set<fs::path> to_be_erase;

	fs::path src       = argc > 1 ? fs::path{argv[1]}    : fs::current_path();
	std::string prefix = argc > 2 ? std::string{argv[2]} : (Network::instance().hostname() + "-");

	std::cout << "Rolling out files prefixed by " << prefix << " in " << src << std::endl;

	auto now = std::chrono::system_clock::now();
	for (const auto& dir : fs::directory_iterator{src})
	{
		if (auto entry = Entry::from_path(prefix, dir.path()); entry)
		{
			put_in_bucket(*entry, year_bucket);
			if (entry->file_time() - now < date::years{1})
				put_in_bucket(*entry, month_bucket);
			if (entry->file_time() - now < date::months{1})
				put_in_bucket(*entry, week_bucket);
			if (entry->file_time() - now < date::weeks{1})
				put_in_bucket(*entry, day_bucket);
			to_be_erase.insert(entry->path());
		}
	}

	for (const auto& [year,entry] : year_bucket)
		to_be_erase.erase(entry.path());
	for (const auto& [month,entry] : month_bucket)
		to_be_erase.erase(entry.path());
	for (const auto& [week,entry] : week_bucket)
		to_be_erase.erase(entry.path());
	for (const auto& [day,entry] : day_bucket)
		to_be_erase.erase(entry.path());

	std::cout << "erasing " << to_be_erase.size() << " files:" << std::endl;
	for (auto&& d : to_be_erase)
	{
	    std::cout << d << (remove(d) ? " success" : " failed") << std::endl;
    }
	return 0;
}
