#include "Network.h"

#include <stdexcept>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <winsock.h>
#else
#include <unistd.h>
#endif

Network::Network()
{
#ifdef WIN32
	WSADATA wsa_data;
	if (int err = WSAStartup(MAKEWORD(2, 2), &wsa_data); err != 0)
		throw std::runtime_error("WSAStartup()");
#endif
}

Network::~Network()
{
#ifdef WIN32
	::WSACleanup();
#endif
}

// Don't make this static otherwise ctor will be skipped.
std::string Network::hostname() const
{
	char hname[80];
	::gethostname(hname, sizeof(hname));
	return hname;
}
